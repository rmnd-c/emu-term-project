﻿// University Term assignment.
//

#include "CMakeProject1.h"
#include <stdio.h>
#define NUM_OF_ELEMENTS 10

using namespace std;


void printArray(int array[], int size);

void reverseValues(int array[], int size);

int calculateSum(int array[], int size);

float calculateAverage(int array[], int size);


int main()
{
	int numbers[NUM_OF_ELEMENTS];
	printf("Please enter %d integer numbers:\n", NUM_OF_ELEMENTS);

	for (int j = 0; j < NUM_OF_ELEMENTS; j++) {
		scanf("%d", &numbers[j]);
	}
	printf("The array contains: ");
	printArray(numbers, NUM_OF_ELEMENTS);

	printf(
		"The sum of the array elements is: %d;\n", calculateSum(numbers, NUM_OF_ELEMENTS)
	);

	printf(
		"The average of the array elements is: %.2f;\n", calculateAverage(numbers, NUM_OF_ELEMENTS)
	);
	reverseValues(numbers, NUM_OF_ELEMENTS);
	printf("Now the array contains: ");
	printArray(numbers, NUM_OF_ELEMENTS);

	return 0;
}

#undef NUM_OF_ELEMENTS



void printArray(int array[], int size) {
	// Procedure to print all array elements. 
	for (int j = 0; j < size; j++) {
		printf("%d, ", array[j]);
	}
	printf("\n");
}


int calculateSum(int array[], int size) {
	// Function to calculate the sum of array elements.
	int sum = 0;
	for (int j = 0; j < size; j++) {
		sum += array[j];
	}
	return sum;
}



float calculateAverage(int array[], int size) {
	// Function to calculate the average array value. 
	// Takes calculateSum function result and processes it.
	int sum = calculateSum(array, size);
	float avg = sum / size;
	return avg;
}


void reverseValues(int array[], int size) {
	// Procedure to reverse an array elements 
	// (reverses not the whole array, but each element separately)
	// Designed to work with 2-digits numbers only.
	int temp;
	printf("Reversing the values.\n");
	for (int j = 0; j < size; j++) {
		temp = array[j] % 10;
		array[j] = 10 * temp + array[j] / 10;
	}
}


